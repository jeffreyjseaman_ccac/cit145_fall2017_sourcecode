//Body Mass Index Calculator
#include <stdio.h>

//function main begins program execution
int main ( void )
{
   // get user's height
   int height; // height of the person
   printf(  "%s", "Please enter your height (in inches): " );
   scanf( "%d", &height );

   // get user's weight
   int weight; // weight of the person 
   printf( "Please enter your weight (in pounds): " );
   scanf( "%d", &weight );

   int BMI; // user's BMI
   BMI = weight * 703 / ( height * height ); // calculate BMI

   printf( "Your BMI is %d\n\n", BMI ); // output BMI

   // output data to user
   puts( "BMI VALUES" );
   puts( "Underweight:\tless than 18.5" );
   puts( "Normal:\t\tbetween 18.5 and 24.9" );
   puts( "Overweight:\tbetween 25 and 29.9" );
   puts( "Obese:\t\t30 or greater" );
} 


