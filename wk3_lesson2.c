
#include <stdio.h>

int main( void )
{ 
   int x; // define first number
   int y; // define second number
   
   printf( "%s", "Enter two numbers: " ); // prompt
   scanf( "%d%d", &x, &y ); // read two integers
   
   // compare the two numbers
   if ( x > y ) {
      printf( "%d is larger\n", x );
   } 

   if ( x < y ) {
      printf( "%d is larger\n", y );
   } 

   if ( x == y ) {
      puts( "These numbers are equal" );
   } 
} 

